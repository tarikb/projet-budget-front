import { Budget, Balance, Expense } from './entities'
import './style.css'

const app = document.querySelector<HTMLDivElement>('#app')!
const budgetTable = document.querySelector<HTMLTableSectionElement>('tbody')!
const expenseTable = document.querySelector<HTMLTableSectionElement>('tbody')!


app.innerHTML = `
  <h1>Gestionnaire de budget</h1>
`

let listBudget: Budget[] = [];
let listExpense: Expense[] = [];
let list: any[] = [];



let bdg = new Budget('', 0, '2022-01-01');
let blc = new Balance("", 0);
let exp = new Expense(0);
const budget = document.querySelector<HTMLElement>('#budget')!;
const balance = document.querySelector<HTMLElement>('#balance')!;
const expense = document.querySelector<HTMLElement>('#expense')!;

// for(let budgettt of list) [
//   budget = budget + budgettt.somme
// ]
budget.textContent = bdg.somme.toString();
balance.textContent = blc.somme.toString();
expense.textContent = exp.somme.toString();

console.log(budget.textContent + ' au chargement de la page');
console.log(balance.textContent + ' au chargement de la page');

document.querySelector<HTMLElement>('#balance')!.classList.add('text-primary');







function addNewBudget() {
  const submitBudget = document.querySelector<HTMLInputElement>('#btnBudget');

  submitBudget?.addEventListener('click', (event) => {

    event.preventDefault();
    checkBalance();
    let inputLabelBudget = document.querySelector<HTMLInputElement>("#inputLabelBudget");
    let inputBudget = document.querySelector<HTMLInputElement>('#positif');
    let inputBudgetDate = document.querySelector<HTMLInputElement>("#budgetDate");

    budget.textContent = parseInt(budget.textContent!) + parseInt(inputBudget?.value!) + ' €';
    balance.textContent = parseInt(balance.textContent!) + parseInt(inputBudget?.value!) + ' €';
    if (inputBudget) {
      addTransaction(inputLabelBudget!.value, parseInt(inputBudget.value,), inputBudgetDate?.value);
      addBudget(inputLabelBudget!.value, parseInt(inputBudget.value,), inputBudgetDate?.value);

    }
    inputLabelBudget!.value = '';
    inputBudget!.value = '';
    // for()
    //   app.innerHTML = `

    // `


    budgetTable.innerHTML = displayFullList();
  });
}

function addNewExpense() {
  const submitExpense = document.querySelector<HTMLInputElement>('#btnExpense');

  submitExpense?.addEventListener('click', (event) => {

    event.preventDefault();
    checkBalance();

    let inputExpense = document.querySelector<HTMLInputElement>('#negatif');
    let inputLabelExpense = document.querySelector<HTMLInputElement>("#inputLabelExpense");
    let inputExpenseDate = document.querySelector<HTMLInputElement>("#ExpenseDate");
    console.log(inputExpense?.value);
    console.log(budget.textContent + ' au click');
    balance.textContent = parseInt(balance.textContent!) - parseInt(inputExpense?.value!);
    expense.textContent = parseInt(expense.textContent!) + parseInt(inputExpense?.value!);
    if (inputExpense) {
      addTransaction(inputLabelExpense!.value, parseInt(inputExpense.value,), inputExpenseDate?.value) + ' €';
      addExpense(inputLabelExpense!.value, parseInt(inputExpense.value,), inputExpenseDate?.value) + ' €';

    }
    console.log(budget.textContent + ' apres assignation');
    inputExpense!.value = '';
    budgetTable.innerHTML = displayFullList();

  });
}

function checkBalance() {
  if (parseInt(balance.textContent!) < 0) {
    document.querySelector<HTMLElement>('#balance')!.classList.add('text-danger');
    console.log('en dessous')
  } else if (parseInt(balance.textContent!) > 0) {
    document.querySelector<HTMLElement>('#balance')!.classList.add('text-success');
    console.log('au dessus')
  }
}




/**
 * Fonction rajoutant une task dans la liste de Task
 * @param label Le label de la task à rajouter dans la liste
 */
function addBudget(label: string, somme: number, date: Date) {
  listBudget.push({
    label: label,
    somme: somme,
    date: date,
  });
  console.log(listBudget + 'liste de budget')
}

function addExpense(label: string, somme: number, date: Date) {
  listExpense.push({
    label: label,
    somme: somme,
    date: date,
  });
  console.log(listExpense + 'liste de budget')
}
function addTransaction(label: string, somme: number, date: Date) {
  list.push({
    label: label,
    somme: somme,
    date: date,
  });
  console.log(listExpense + 'liste de budget')
}

function showBudgetList() {
  const showBudget = document.querySelector<HTMLInputElement>('#displayBudgetList');

  showBudget?.addEventListener('click', (event) => {

    event.preventDefault();
    budgetTable.innerHTML = displayBudgetList();

  });
}

function showExpenseList() {
  const showBudget = document.querySelector<HTMLInputElement>('#displayExpenseList');

  showBudget?.addEventListener('click', (event) => {

    event.preventDefault();
    budgetTable.innerHTML = displayExpenseList();

  });
}

function displayBudgetList(): string {

  let display = '';
  for (const budget of listBudget) {
    display += `
    <tr class="table-success">
    <th scope="row"></th>
    <td> ${budget.label}</td>
    <td> ${budget.somme} €</td>
    <td> ${budget.date}</td>
    </tr>
    `


  }
  return display;
}

function displayExpenseList(): string {

  let display = '';
  for (const expense of listExpense) {
    display += `
    <tr class="table-danger">
    <th scope="row"></th>
    <td> ${expense.label}</td>
    <td> ${expense.somme} €</td>
    <td> ${expense.date}</td>
    </tr>
    `


  }
  return display;
}

addNewBudget();

addNewExpense();
showBudgetList();
showExpenseList();


function displayFullList() {
  let display = '';
  for (const fullList of list) {

    display += `
    <tr>
    <th scope="row"></th>
    <td> ${fullList.label}</td>
    <td> ${fullList.somme} €</td>
    <td> ${fullList.date}</td>
    </tr>
    `
  }
  console.log(list);
  return display;

}
budget.textContent = bdg.somme.toString();
balance.textContent = blc.somme.toString();
expense.textContent = exp.somme.toString();