export class Budget {
    // id: number;
    label: string;
    somme: number;
    date: Date;


    constructor(label: string, somme: number, date: Date) {
        // this.id = id;
        this.label = label;
        this.somme = 0;
        this.date = date;
    }
}

export class Balance {
    // id: number;
    label: string;
    somme: number;



    constructor(label: string, somme: number) {
        // this.id = id;
        this.label = label;
        this.somme = 0;
    }
}

export class Expense {
    // id: number;
    label: string;
    somme: number;
    date: Date;



    constructor(label: string, somme: number, date: Date) {
        // this.id = id;
        this.label = label;
        this.somme = 0;
        this.date = date;
    }
}