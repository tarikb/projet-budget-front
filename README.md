# Projet Budget Front

Ce projet est une application de gestion de comptes. Il est constitué du projet et d’une maquette représentant l’application.

![Maquette projet budget](maquette_projet_budget.drawio.svg "Maquette du projet ")





## Fonctionnalitées

L’application se comporte de plusieurs éléments : 
- Le budget : Représente les entrées d’argents ajoutées par l’utilisateur,
- La dépense : Représente les dépenses ajoutées par l’utilisateur.
- La balance : La balance est le résultat des entrées et de la balance. Il peut être positif ou bien négatif en fonction des entrées de l’utilisateur.

L’application est constituée de 2 formulaires :
- Un formulaire est constitué d’un label, de la somme et de la date d’entrée permettant d’ajouter une entrée.
- Un formulaire est constitué d’un label, de la somme et de la date d’entrée permettant d’ajouter une dépense.
- La balance est calculé automatiquement en fonction des entrées de l’utilisateur.

Dans les 2 formulaire un bouton valider permet d’enregistrer son entrée.
Lorsque celle ci est effectuée, l’entrée est ajouter à un tableau d’entrée.
Ce tableau Afficher dans l’ordre les entrée effectuées que ce soit des rentrés d’argent ou bien des dépenses.
Dans chaque formulaire un bouton permet respectivement d’afficher uniquement la lise des rentrées d’argent ou bien celle des dépenses

## Problèmes rencontrés

Différents problèmes ont été rencontrés,  bien que le code est «fonctionnel» certaines lignes dans mon code apparaissent en rouge et je n’ai malheureusement pas pu en trouver la solution.
J’ai codé une fonctionnalité me permettant d’ajouter un id à chaque entrées afin que celles ci soit numérote, mais je l’ai malheureusement supprimer car les id ajouté étais incrémenter à l’envers (la 1ere entrée obtenais l’id correspondant à la nouvelle entrée).
Une fonctionnalité me permettant de changé la classe css de la balance contient un bug. Celle ci est censé s’afficher en bleu lorsqu’elle est à 0, en vert lorsqu’elle est positive et en rouge lorsqu’elle est négative.



## Futurs mise à jour éventuelles

- Améliorer le front afin de le rendre plus attrayant.
- incrémenter un id.
- Afficher en haut du tableau la dernière entrée (inverser l’affichage du tableau à chaque entrées),
- Ajouter un système de filtre en fonction de la date comme par exemple afficher dans le tableau uniquement les opérations à un jour une période précise (affichage par mois).
- Appliquer les correctifs indiqué dans les problèmes rencontrés.
